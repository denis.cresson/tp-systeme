## Sujet abordé

 utilisateurs groupes et permissions

## Objectifs

Comprendre le role des  permissions  et leurs interactions avec les utilisateurs et les groupes (savoir modifier celles-ci)

## Pré-requis

bases de l'utilisation d'un terminal

déplacement dans les repertoires 

affichage du contenu des repertoires

création d'un fichier avec touch

écriture dans un fichier avec vim



## Elements de cours

### utilisateurs et groupe

 ajout d'un utilisateur (sudo adduser)

ajout d'un groupe (sudo addgroup)

ajout d'un utilisateur dans un groupe ( sudo adduser utilisateur groupe)

changer d'utilisateur dans un terminal (su - utilisateur)

affichage  de l'identifiant d'utilisateur (uid) groupe principal (gip) et autres groupes auquel l'utilisateur appartient

### Permissions

affichage des permissions avec ls -l

modification des permissions d'un fichier  

    chmod  en choisissant 

* celui à qui s'applique le changement (u,g,o,a)

* la modification (+ - =)

* le droit (r,w,x) 



## Séance pratique

### Utilisateurs et groupes

- créer deux utilisateurs guest1 et guest2

- créer un groupe g1

- mettre guest1 dans g1

- ajoutez vous a g1

### Identifiants et groupes

* changer d'utilisateur et passer à guest1

* afficher son identifiant et les groupes auquels il appartient

* même chose pour guest2

* afficher votre identifiant et les groupes auquels vous appartenez

### Fichiers et permissions

* créer un fichier mien dans votre repertoire home (le votre)

* changer d'utilisateur (guest1)

* créer un fichier tien dans le repertoire home (celui de guest1)

### Affichage des permissions

* changer d'utilisateur (vous)

* afficher les permissions de mien

* afficher les permissions de tien

* ouvrir mien et y écrire moi

* faire de même avec tien et toi (expliquer)

### Modifier les permissions

* modifier les permissions de tien pour que vous et guest2 puissent y accéder en écriture

* modifier les permissions de tien pour que vous puissiez y accéder en écriture mais pas guest2



### QCM E3C
j'ai créé un fichier fich dans mon répertoire vide ici, que va afficher la commande ls -l si je suis dans ce repertoire (mon nom d'utilisateur est moi)
* drwxrw-rw- 1 moi moi juin 18 21:19 fich
* -rw-rw-r-- 1 moi moi juin 18 21:19 fich
* -rw-rw-rw- 1 moi moi juin 18 21:19 fich
* -r--r--r-- 1 moi moi juin 18 21:19 fich 

les permissions d'un fichier sont rw-rw-rw- que deviennent-elles après l'instruction chmod g+x
* rwxrw-rw-
* rw-rwxrw-
* rw-rw-rwx
* rwxrwxrwx

